#define CATCH_CONFIG_RUNNER 1
#include <catch2/catch.hpp>

#include <mn/OS.h>
#include <mn/Memory.h>

int main(int argc, char** argv)
{
	mn::allocator_push(mn::leak_detector());
	return Catch::Session().run( argc, argv );
}