
#include <catch2/catch.hpp>

#include <break/loom/Group.h>
#include <break/loom/Loom.h>
#include <break/loom/Functional.h>

#include <mn/Buf.h>
#include <mn/IO.h>
#include <mn/Thread.h>

#include <atomic>
#include <chrono>
#include <stdint.h>

using namespace brk::loom;
using namespace mn;

TEST_CASE("worker", "loom")
{
	SECTION("Case 01")
	{
		Worker w1 = worker_new("1");
		worker_free(w1);
	}

	SECTION("Case 02")
	{
		Group g = group_new("G", 3);

		std::atomic<size_t> counter = 0;
		auto increment = [](void* user_data, void* b) -> void {
			std::atomic<size_t>* self = (std::atomic<size_t>*)user_data;
			++(*self);
		};

		Job p = job_new(group_push_next(g), nullptr, nullptr, nullptr, "parent", nullptr);
		for(size_t i = 0; i < 100; ++i)
		{
			Job c = job_new(group_push_next(g), increment, &counter, nullptr, "child", p);
			job_schedule(c);
			job_free(c);
		}

		job_schedule(p);
		job_wait(p);
		job_free(p);

		CHECK(counter == 100);

		group_free(g);
	}

	SECTION("Case 03")
	{
		Loom l = loom_new("L", 3);

		Buf<Request*> reqs = buf_new<Request*>();

		std::atomic<size_t> counter = 0;
		for(size_t i = 0; i < 100; ++i)
		{
			if(i % 3 == 0)
			{
				request_sync(l, [&counter](){
					counter++;
				});
			}
			else
			{
				buf_push(reqs, request_async(l, [&counter](){
					counter++;
				}));
			}
		}

		for(Request* r: reqs)
			request_free(r);

		CHECK(counter == 100);

		buf_free(reqs);
		loom_free(l);
	}

	SECTION("Case 04")
	{
		Loom l = loom_new("L", 3);

		Buf<int> nums = buf_new<int>();
		for(int i = 0; i < 100000; ++i)
			buf_push(nums, i);

		map(l, nums, [](int& num){
			++num;
		});

		for(int i = 0; i < 1000; ++i)
			CHECK(nums[i] == i + 1);

		buf_free(nums);

		loom_free(l);
	}

	SECTION("Case 04")
	{
		Loom l = loom_new("L", 3);

		Buf<int64_t> nums = buf_new<int64_t>();
		for(int64_t i = 1; i <= 100000; ++i)
			buf_push(nums, i);

		int64_t res = reduce(l, nums, int64_t(0), [](int64_t a, int64_t b){return a + b;});

		CHECK(res == 5000050000);

		buf_free(nums);

		loom_free(l);
	}
}
