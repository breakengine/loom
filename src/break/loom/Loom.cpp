#include "break/loom/Loom.h"

#include <mn/Memory.h>
#include <mn/Pool.h>
#include <mn/Thread.h>
#include <mn/Buf.h>

namespace brk::loom
{
	using namespace mn;

	constexpr static size_t REQUEST_POOL_SIZE = 1024;

	struct ILoom
	{
		Group group;
		Pool pool;
		Mutex pool_mtx;
		Buf<Request*> gc;
		Mutex gc_mtx;
	};

	Loom
	loom_new(const char* name, size_t worker_count)
	{
		ILoom* self = alloc<ILoom>();
		self->group = group_new(name, worker_count);
		self->pool = pool_new(SMALL_REQUEST_SIZE, REQUEST_POOL_SIZE);
		self->pool_mtx = mutex_new(name);
		self->gc = buf_new<Request*>();
		self->gc_mtx = mutex_new(name);
		return (Loom)self;
	}

	void
	loom_free(Loom loom)
	{
		ILoom* self = (ILoom*)loom;
		group_free(self->group);

		for(Request* r: self->gc)
			if(r->loom == nullptr)
				mn::free(r);

		pool_free(self->pool);
		mutex_free(self->pool_mtx);

		buf_free(self->gc);
		mutex_free(self->gc_mtx);

		free(self);
	}

	Group
	loom_group(Loom loom)
	{
		ILoom* self = (ILoom*)loom;
		return self->group;
	}

	void*
	loom_alloc(Loom loom)
	{
		ILoom* self = (ILoom*)loom;
		mutex_lock(self->pool_mtx);
			void* res = pool_get(self->pool);
		mutex_unlock(self->pool_mtx);
		return res;
	}

	void
	loom_free(Loom loom, Request* request)
	{
		ILoom* self = (ILoom*)loom;
		mutex_lock(self->gc_mtx);
			buf_push(self->gc, request);
		mutex_unlock(self->gc_mtx);
	}

	void
	loom_gc(Loom loom)
	{
		ILoom* self = (ILoom*)loom;
		bool do_gc = false;
		mutex_lock(self->gc_mtx);
		if(self->gc.count >= REQUEST_POOL_SIZE)
		{
			do_gc = true;
			buf_remove_if(self->gc, [self](Request* r){
				if(job_done(r->job) == true)
				{
					job_free(r->job);
					r->~Request();

					if(r->loom == nullptr)
					{
						free(r);
					}
					else
					{
						mutex_lock(self->pool_mtx);
							pool_put(self->pool, r);
						mutex_unlock(self->pool_mtx);
					}

					return true;
				}
				return false;
			});
		}
		mutex_unlock(self->gc_mtx);
		if(do_gc) group_gc(self->group);
	}
}
